# bun slack error

This is a repo to reproduce an error with the [bun](https://bun.sh) runtime with Slack apps.

I believe the error occurs in the @slack/oauth repo.

## Steps to reproduce

# Setup

0. Set up ngrok
 - Run `ngrok http 3000`
 - You will use the https url that ngrok gives you
1. Clone this repo
2. Run `npm install`
 - Tested using node:20.9.0 and npm:10.1.0
3. Utilize your ngrok url
 - Either set the `SLACK_BASE_URL` environment variable to your ngrok url when running manifest.js,
 - OR, set the `baseUrl` variable in manifest.js to your ngrok url 
3. Run `node manifest.js`
 - This will log json you will use in the next step
4. Create a Slack app
 - Go to https://api.slack.com/apps?new_app=1, create a slack app from a manifest, and paste the json from the previous step
5. Install the app to your workspace

# Baseline running node

6. Run `node index.js`
7. Go to your ngrok url /slack/install
 - You should see a button to install the app
 - You should successfully go to a slack install screen

# Error with bun

8. Run `rm -rf node_modules`
9. Run `bun index.js`
10. Go to your ngrok url /slack/install
  - you won't even get a page load, you will get an error in the console of

```
[ERROR]  OAuth:InstallProvider:0 An unhandled error occurred while processing an install path request (error: TypeError: Right hand side of instanceof is not an object)
[ERROR]   An unhandled error occurred while Bolt processed a request to the installation path (Right hand side of instanceof is not an object)
[DEBUG]   Error details: Error: Right hand side of instanceof is not an object
```