const baseUrl = process.env.SLACK_BASE_URL;
// const baseUrl = "https://YOUR-OWN-URL.ngrok-free.app";

const manifest = {
  "display_information": {
      "name": "bun-slack-error",
  },
  "oauth_config": {
      "redirect_urls": [
        baseUrl + "/slack/oauth_request"
      ]
  },
  "settings": {
      "org_deploy_enabled": false,
      "socket_mode_enabled": false,
      "token_rotation_enabled": false
  }
};

console.log(JSON.stringify(manifest));